package br.lembretes.model;

public class Lembrete {
	private Integer id;
	private String descricao;
	
	
	public Lembrete() {
		this.id = -1;
		this.descricao = "";
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
