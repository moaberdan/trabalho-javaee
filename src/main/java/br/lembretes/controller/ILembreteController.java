package br.lembretes.controller;


import java.util.Collection;
import java.util.List;

import javax.faces.model.DataModel;

import br.lembretes.model.Lembrete;

public interface ILembreteController {
	public String inserirLembrete();
	public String removerLembrete(Lembrete lembrete);
	public String alterarLembrete();
	public Lembrete listarLembrete(Lembrete lembrete);
	public Collection<Lembrete> listarLembretes();
}
