package br.lembretes.controller;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.lembretes.model.Lembrete;
import br.lembretes.services.ILembreteService;
import br.lembretes.services.LembreteService;

@ManagedBean(name="lembreteController")
@SessionScoped
public class LembreteController implements ILembreteController{
	private Lembrete lembrete;
	private ILembreteService lembreteService;
	private Collection<Lembrete> lembretes;
	private boolean salvar;
	
	public LembreteController() {
		this.lembreteService = new LembreteService();
		this.salvar = true;
	}

	@Override
	public String inserirLembrete() {
		try {
			this.lembreteService.inserirLembrete(lembrete);
			this.lembrete = new Lembrete();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "index";
		
	}

	@Override
	public String removerLembrete(Lembrete _lembrete) {
		try {
			this.lembreteService.removerLembrete(_lembrete);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "index";
	}

	@Override
	public String alterarLembrete() {
		try {
			this.lembreteService.alterarLembrete(this.lembrete);
			this.lembrete = new Lembrete();
			this.salvar = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "index";
	}
	
	public String alterarAction(Lembrete _lembrete) {
		this.lembrete = _lembrete;
		this.salvar = false;
		return "index";
	}
	@Override
	public Lembrete listarLembrete(Lembrete _lembrete) {
		try {
			return this.lembreteService.listarLembrete(_lembrete);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Collection<Lembrete> listarLembretes() {
		return this.lembreteService.listarLembretes();
	}
	@PostConstruct
    public void init() {
		lembrete = new Lembrete();
    }
	
	public Lembrete getLembrete() {
		return lembrete;
	}

	public void setLembrete(Lembrete lembrete) {
		this.lembrete = lembrete;
	}
	
	public Collection<Lembrete> getLembretes() {
		this.lembretes = this.lembreteService.listarLembretes();
		
		
		return this.lembretes;
	}
	
	public void setLembretes(Collection<Lembrete> lembretes) {
		this.lembretes = lembretes;
	}

	public boolean isSalvar() {
		return salvar;
	}

	public void setSalvar(boolean salvar) {
		this.salvar = salvar;
	}
}
