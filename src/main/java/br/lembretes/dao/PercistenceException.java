package br.lembretes.dao;

public class PercistenceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PercistenceException(String msg) {
		super(msg);
	}
}
