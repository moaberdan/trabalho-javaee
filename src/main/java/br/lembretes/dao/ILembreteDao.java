package br.lembretes.dao;


import java.util.Collection;
import java.util.List;

import javax.faces.model.DataModel;

import br.lembretes.model.Lembrete;

public interface ILembreteDao {
	public void inserirLembrete(Lembrete lembrete) throws PercistenceException;
	public void removerLembrete(Lembrete lembrete) throws PercistenceException;
	public void alterarLembrete(Lembrete lembrete) throws PercistenceException;
	public Lembrete listarLembrete(Lembrete lembrete) throws PercistenceException;
	public Collection<Lembrete> listarLembretes();
	public boolean valida(Lembrete lembrete);
}
