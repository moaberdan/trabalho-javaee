package br.lembretes.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.model.DataModel;

import br.lembretes.model.Lembrete;

public class LembreteDaoMemoria implements ILembreteDao{
	private Integer index;
	private Map<Integer, Lembrete> lembretes;
	
	public LembreteDaoMemoria() {
		System.out.println("Dao iniciado");
		this.lembretes = new HashMap<Integer, Lembrete>();
		this.index = 1;
		/*Lembrete l = new Lembrete();
		l.setId(1);
		l.setDescricao("teste");
		this.lembretes.put(l.getId(), l);
		
		Lembrete l2 = new Lembrete();
		l2.setId(2);
		l2.setDescricao("teste2");
		this.lembretes.put(l2.getId(), l2);
		
		Lembrete l3 = new Lembrete();
		l3.setId(3);
		l3.setDescricao("teste3");
		this.lembretes.put(l3.getId(), l3);*/
	}
	@Override
	public void inserirLembrete(Lembrete lembrete) throws PercistenceException {
		lembrete.setId(this.index);
		if(!this.valida(lembrete)) {
			this.lembretes.put(this.index, lembrete);
			this.index++;
		}else {
			throw new PercistenceException("Lembrete existente");
		}
	}
	

	@Override
	public void removerLembrete(Lembrete lembrete) throws PercistenceException {
		if(this.valida(lembrete)) {
			this.lembretes.remove(lembrete.getId());
		}else {
			throw new PercistenceException("Lembrete não encontrado");
		}
		
	}


	@Override
	public void alterarLembrete(Lembrete lembrete) throws PercistenceException {
		if(this.valida(lembrete)) {
			this.lembretes.get(lembrete.getId()).setDescricao(lembrete.getDescricao());
		}else {
			throw new PercistenceException("Lembrete não encontrado");
		}
		
	}


	@Override
	public  Lembrete listarLembrete(Lembrete lembrete) throws PercistenceException {
		if(this.valida(lembrete)) {
			return this.lembretes.get(lembrete.getId());
		}else {
			throw new PercistenceException("Lembrete não encontrado");
		}
		
	}


	@Override
	public Collection<Lembrete> listarLembretes() {
		return this.lembretes.values();		
	}
	
	public boolean valida(Lembrete lembrete) {
		return this.lembretes.containsKey(lembrete.getId());
	}

}
