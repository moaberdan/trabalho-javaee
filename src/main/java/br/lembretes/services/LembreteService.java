package br.lembretes.services;

import java.util.Collection;

import br.lembretes.dao.ILembreteDao;
import br.lembretes.dao.LembreteDaoMemoria;
import br.lembretes.model.Lembrete;

public class LembreteService implements ILembreteService {

	private ILembreteDao lembreteDao;
	
	public LembreteService() {
		this.lembreteDao = new LembreteDaoMemoria();
	}
	@Override
	public void inserirLembrete(Lembrete lembrete) throws BusinessException {
		try {
			this.lembreteDao.inserirLembrete(lembrete);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		
	}

	@Override
	public void removerLembrete(Lembrete lembrete) throws BusinessException {
		try {
			this.lembreteDao.removerLembrete(lembrete);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		
	}

	@Override
	public void alterarLembrete(Lembrete lembrete) throws BusinessException {
		try {
			this.lembreteDao.alterarLembrete(lembrete);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		
	}

	@Override
	public Lembrete listarLembrete(Lembrete lembrete) throws BusinessException {
		try {
			return this.lembreteDao.listarLembrete(lembrete);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public Collection<Lembrete> listarLembretes() {
		
		return this.lembreteDao.listarLembretes();
	}


}
