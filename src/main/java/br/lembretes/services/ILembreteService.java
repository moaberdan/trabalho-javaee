package br.lembretes.services;
import java.util.Collection;

import br.lembretes.model.Lembrete;

public interface ILembreteService {
	public void inserirLembrete(Lembrete lembrete) throws BusinessException;
	public void removerLembrete(Lembrete lembrete) throws BusinessException;
	public void alterarLembrete(Lembrete lembrete) throws BusinessException;
	public Lembrete listarLembrete(Lembrete lembrete) throws BusinessException;
	public Collection<Lembrete> listarLembretes();
}
